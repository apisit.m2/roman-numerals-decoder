package main

import (
	"testing"
)

func TestDecode(t *testing.T) {
	tests := []struct {
		roman string
		want  int
	}{
		{"I", 1},
		{"i", 1},
		{"II", 2},
		{"ii", 2},
		{"III", 3},
		{"iIi", 3},
		{"IV", 4},
		{"iv", 4},
		{"V", 5},
		{"v", 5},
		{"VI", 6},
		{"VII", 7},
		{"VIII", 8},
		{"IX", 9},
		{"iX", 9},
		{"X", 10},
		{"XIV", 14},
		{"XVIII", 18},
		{"XIX", 19},
		{"XXX", 30},
		{"XL", 40},
		{"LXXX", 80},
		{"XC", 90},
		{"XCvi", 96},
		{"C", 100},
		{"CXXXVIII", 138},
		{"CD", 400},
		{"CDLXXXVI", 486},
		{"D", 500},
		{"DCCLIV", 754},
		{"DCCCLXIX", 869},
		{"CM", 900},
		{"CMLXVII", 967},
		{"M", 1000},
		{"MMXXIIII", 2024},
		{"MMDLXxiii", 2573},
		{"_V", 5000},
		{"_VC", 5100},
		{"_VCMLXIII", 5963},
		{"_V_I_IXXIV", 7024},
		{"_V_I_ICMXCIX", 7999},
		{"_X", 10000},
		{"_X_I_XDCCCLXV", 19865},
		{"_X_X_X_V_IDCCLXXX", 36780},
		{"_L", 50000},
		{"_L_X_X_I_XDCCCXII", 79812},
		{"_C", 100000},
		{"_C_X_X_X_V_IDCCLXXX", 136780},
		{"_C_C_C_L_X_I_I_ICCXCVIII", 363298},
		{"_D", 500000},
		{"_D_C_C_C_L_X_I_I_ICCXCVIII", 863298},
		{"_M", 1000000},
		{"_M_D_C_C_C_L_X_I_I_ICCXCVIII", 1863298},
		{"_M_M_D_C_C_X_L_V_I_I_ICMXXXII", 2748932},
		{"_M_M_M_C_M_X_C_I_XCMXCIX", 3999999},
	}

	for _, tt := range tests {
		t.Run(tt.roman, func(t *testing.T) {
			got, err := Decode(tt.roman)
			if err != nil {
				t.Errorf("Decode(%s) got error %v", tt.roman, err)
			}
			if got != tt.want {
				t.Errorf("Decode(%s) = %d; want %d", tt.roman, got, tt.want)
			}
		})
	}
}

func TestDecodeError(t *testing.T) {
	tests := []struct {
		roman string
		want  string
	}{
		{"INVALID", "invalid roman number: a"},
		{"IVo", "invalid roman number: o"},
		{"mCMiVnnnn", "invalid roman number: n"},
		{"CXVSDGWRYWE", "invalid roman number: e"},
	}
	for _, tt := range tests {
		t.Run(tt.roman, func(t *testing.T) {
			_, err := Decode(tt.roman)
			if err == nil {
				t.Errorf("Decode(%s) should return an error", tt.roman)
			}
			if err.Error() != tt.want {
				t.Errorf("Decode(%s) = %v; want %v", tt.roman, err, tt.want)
			}
		})
	}
}
