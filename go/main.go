package main

import (
	"errors"
	"fmt"
	"strings"
)

func main() {
	result, err := Decode("mCMiVnnnn")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(result)
}

func Decode(roman string) (int, error) {
	roman = strings.ToLower(roman)
	var result, previousVal, currentVal int

	romanNumbers := map[string]int{
		"i":  1,
		"v":  5,
		"x":  10,
		"l":  50,
		"c":  100,
		"d":  500,
		"m":  1000,
		"_i": 1000,
		"_v": 5000,
		"_x": 10000,
		"_l": 50000,
		"_c": 100000,
		"_d": 500000,
		"_m": 1000000,
	}

	for i := len(roman) - 1; i >= 0; i-- {
		if _, ok := romanNumbers[string(roman[i])]; !ok {
			return 0, errors.New("invalid roman number: " + string(roman[i]))
		}
		if i > 0 && roman[i-1] == '_' {
			currentVal = romanNumbers[string(roman[i-1])+string(roman[i])]
			i--
		} else {
			currentVal = romanNumbers[string(roman[i])]
		}
		if currentVal < previousVal {
			result -= currentVal
		} else {
			result += currentVal
		}

		previousVal = currentVal
	}

	return result, nil
}
